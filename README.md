# This repo is Work In Progress

This is RnD project you can follow on [Telegram](https://t.me/devops_garage) or [Youtube](https://www.youtube.com/channel/UCgYAre_aCS9swWL8ZDbOAbw)

# Description

Ansible playbook for Opensearch and Opensearch Dashboards installation in Yandex.Cloud.

# Quickstart

1. Create [OAuth token for Yandex.Cloud](https://cloud.yandex.com/en/docs/iam/concepts/authorization/oauth-token).
2. Put your token into `yacloud_token` variable in `environments/yacloud-test/yacloud_compute.yml`
3. Create VM with label `group = opensearch`
4. Run command
```bash
ansible-playbook -i environments/yacloud-test/yacloud_compute.yml site.yml
```

## !!!WARNING!!!
Don't store `yacloud_token` value as plain text. I recommend you to crypt it using `ansible-vault`.

1. Create `vault.key` with long random string as contents.
2. Encypt your token with command
```bash
ansible-vault encrypt_string '<oauth_token>' --name yacloud_token
```
3. Write output into environment spec file.

# Testing with elasticsearch-stress-test
[Source Repo](https://github.com/logzio/elasticsearch-stress-test)

1. Install `elasticsearch-stress-test`
```bash
git clone https://github.com/logzio/elasticsearch-stress-test.git
pip install opensearch-py
```
2. Substitue `elasticsearch` lib with `opensearch-py`
3. Run `elasticsearch-stress-test`
```bash
cd elasticsearch-stress-test
python3 elasticsearch-stress-test.py --es_address https://<server_ip>:9200 --no-verify --username admin --password admin --indices 4 --documents 5 --seconds 120 --not-green --clients 5
```

# Testing with ESRally
ESRally [Documentation](https://esrally.readthedocs.io/en/stable/index.html) | [Source Code](https://github.com/elastic/rally)

## Installing
1. Install `esrally` and `opensearch-py` with `pip`
```bash
sudo apt update
sudo apt install git python3-pip
pip3 install opensearch-py esrally
```
2. Change working directory to `esrally` package directory. For local installation it looks like this:
```bash
cd ~/.local/lib/python3.8/site-packages/esrally
```
3. Substitue `elasticsearch` lib with `opensearch-py`
```bash
grep -rl 'elasticsearch' . | xargs sed -i 's#import elasticsearch#import opensearchpy#g'
grep -rl 'elasticsearch' . | xargs sed -i 's#except elasticsearch#except opensearchpy#g'
grep -rl 'elasticsearch' . | xargs sed -i 's#from elasticsearch#from opensearchpy#g'
grep -rl 'elasticsearch' . | xargs sed -i 's#elasticsearch.Elasticsearch#opensearchpy.OpenSearch#g'
grep -rl 'elasticsearch' . | xargs sed -i 's#elasticsearch.AIOHttpConnection#opensearchpy.AIOHttpConnection#g'
grep -rl 'elasticsearch' . | xargs sed -i 's#elasticsearch.AsyncTransport#opensearchpy.AsyncTransport#g'
grep -rl 'elasticsearch' . | xargs sed -i 's#elasticsearch.AsyncElasticsearch#opensearchpy.AsyncOpenSearch#g'
grep -rl 'Elasticsearch' . | xargs sed -i 's#ElasticsearchException#OpenSearchException#g'
```
4. Change minimal acceptable cluster version 
```bash
echo '1.0.0' > min-es-version.txt
```

## Prepare test environment
1. Download opensearch certificate from server
```bash
scp <server_user>@<server_ip>:/opt/opensearch-1.0.1/config/root-ca.pem ./
```
2. Upload certificate to esrally host
```bash
scp root-ca.pem ubuntu@<esrally_ip>:/home/ubuntu/
```
3. Add hostname on esrally host
```bash
echo '<local_server_ip> node-0.example.com' >> /etc/hosts
```

## Running
```bash
esrally race --track=geonames \
	--pipeline=benchmark-only \
	--target-hosts=node-0.example.com:9200 \
	--client-options=timeout:60,use_ssl:true,verify_certs:false,basic_auth_user:'admin',basic_auth_password:'admin',ca_certs:'/home/ubuntu/root-ca.pem'
```

## Logging
```bash
export THESPLOG_FILE="${HOME}/.rally/logs/actor-system-internal.log"
export THESPLOG_FILE_MAXSIZE="204800"
export THESPLOG_THRESHOLD="INFO"
rm -rf ~/.rally/logs/rally.log
```

# Testing with flog and vector
1. Download `flog`
```bash
curl -L -o - https://github.com/mingrammer/flog/releases/download/v0.4.3/flog_0.4.3_linux_amd64.tar.gz | tar -xzf -
```
2. Run `flog` with output to vector
```
flog -f json -n 1000000 | sudo -u vector vector
```
